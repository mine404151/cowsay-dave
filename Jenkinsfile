pipeline {
    agent any

    options {
        timeout(time: 5, unit: "MINUTES")
        timestamps()
    }

    environment {
        AWS_ECR = "644435390668.dkr.ecr.ap-south-1.amazonaws.com/dave-ecr:latest"
        RUNTIME_ENV_IP = "13.233.119.154"
        RUNTIME_ENV_USER = "ubuntu@${RUNTIME_ENV_IP}"
        RUNTIME_PORT = "3000"
    }

    stages {
        stage("Build") {
            steps {
                sh("docker build -t cowsay:latest .")
            }
        }

        stage("Publish") {
            steps {
                sh("aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin 644435390668.dkr.ecr.ap-south-1.amazonaws.com")
                sh("docker tag cowsay:latest ${AWS_ECR}")
                sh("docker push ${AWS_ECR}")
            }
        }

        stage("Deploy") {
            when {
                allOf {
                    expression { 
                        echo "Triggered by user: ${currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause) != null}"
                        currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause) != null 
                    }
                    anyOf {
                        branch 'main'
                        branch 'staging'
                    }
                }
            }
            steps {
                script {
                    def portToExpose
                    def branchName = env.BRANCH_NAME

                    if (branchName == 'main') {
                        portToExpose = 80
                    } else if (branchName == 'staging') {
                        portToExpose = 3000
                    } else {
                        error("Unsupported branch: ${branchName}")
                    }

                    sh("""
                        ssh -i /var/jenkins_home/dve-pair.pem -o "StrictHostKeyChecking no" ${RUNTIME_ENV_USER} \
                        'aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin ${AWS_ECR} && \
                        sleep 5s && \
                        docker pull ${AWS_ECR} && \
                        sleep 5s && \
                       
                        docker run -d -p ${portToExpose}:8080 --name cowsay ${AWS_ECR}'
                        sleep 5s
                    """)
                }
            }
        }

        stage("Feature Test") {
            when {
                branch 'feature/*'
            }
            steps {
                sh("""
                    docker rm -f cowsay  && \
                    docker run -d -p ${RUNTIME_PORT}:8080 --name cowdave cowsay:latest
                    sleep 15s
                    curl http://ddve.strangled.net:3000/

                """)
            }
        }

        stage("E2E test") {
            when {
                anyOf {
                    branch 'main'
                    branch 'staging'
                }
            }
            steps {
                timeout(time: 15, unit: "SECONDS") {
                    sh("""
                        until curl --include ${env.RUNTIME_ENV_IP}:${RUNTIME_PORT} | head -n 1 | grep '200 OK'
                        do
                            sleep 1
                        done
                    """)
                }
            }
        }
    }

    post {
        always {
            sh("""
                ssh -i /var/jenkins_home/dve-pair.pem -o "StrictHostKeyChecking no" ${RUNTIME_ENV_USER} \
                'docker rm -f cowsay'
            """)
        }
        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }
    }
}
